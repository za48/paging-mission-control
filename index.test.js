const index = require('./index');

test("Testing readComponent from data", () => {
    expect(index.readComponent("20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT")).toBe("BATT");
});

test("Testing readComponent from data", () => {
  expect(index.readComponent("20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT")).toBe("TSTAT");
});

test("Testing readRawValue from data", () => {
  expect(index.readRawValue("20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT")).toBe("7.9");
});

test("Testing readRawValue from data", () => {
  expect(index.readRawValue("20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT")).toBe("89.9");
});

