const Satellite = require('./satellite.js');
const fs = require('fs');

/** Const battLimit: red low limit for battery voltage readings. */
const battLimit = 8;
module.exports.battLimit = battLimit;

/** Const tstatLimit: red high limit for thermostat readings. */
const tstatLimit = 101;
module.exports.tstatLimit = tstatLimit;

/** Const satellite: objects representing each satellite and its data. */
const satellite1000 = new Satellite(1000);
const satellite1001 = new Satellite(1001);

/** Function readComponent: accepts status telemetry data and parses out component text. */
function readComponent(line) {
  let index = 0;
  for (let i = 0; i < 7; i++) {
    index = line.indexOf("|", index+1)
  }
  return line.substring(index+1);
}
module.exports.readComponent = readComponent;

/** Function readRawValue: accepts status telemetry data and parses out rawValue text. */
function readRawValue(line) {
  let index = 0;
  for (let i = 0; i < 6; i++) {
    index = line.indexOf("|", index+1)
  }
  return line.substring(index+1, line.indexOf("|", index+1));
}
module.exports.readRawValue = readRawValue;

/** Const loadInput: reads input status telemetry data file and populates status 
 *  logs of satellite objects. 
 *  Input is entered in file: './input.txt'
 */
    const loadInput = fs.readFileSync('./input.txt', 'utf-8');
    loadInput.split(/\r?\n/).forEach((line) => {
    line.toString();
    const timestamp = line.substring(0, 21);
    const satellite = (line.substring(22, 26) == "1000") ? satellite1000 : satellite1001;

    const rawVal = readRawValue(line);
    const component = readComponent(line);

    satellite.addStatus(component, timestamp, rawVal)
});