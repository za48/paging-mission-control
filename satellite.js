const index = require('./index.js');

/** Class Satellite is a class representing one satellite and its telemetry data. 
 *  It has properties: String id for satellite id; Map battStatusLog as a map
 *  of timestamps to battery voltage readings; Map tstatStatusLog as a map of timestamps
 *  to thermostat readings; list battViolations as a list of battery violation cases; 
 *  list tstatViolations as a list of thermostat violation cases; list alerts for 
 * alerts that came up.
 */
class Satellite {
  constructor(id) {
    this.id = id;
    this.battStatusLog = new Map();
    this.tstatStatusLog = new Map();
    this.battViolatons = [];
    this.tstatViolations = [];
    this.alerts = [];
  }

  

  /** Function addStatus: Adds a battery/thermostat status timestamp and rawValue
   *  to its corresponding status log. Accepts the String component name, String 
   *  timestamp, and rawValue. */ 
  addStatus(component, timestamp, rawValue) {
    let time = new Date(
      timestamp.substring(0, 4), 
      timestamp.substring(4, 6), 
      timestamp.substring(6, 8), 
      timestamp.substring(9, 11), 
      timestamp.substring(12, 14), 
      timestamp.substring(15, 17), 
      timestamp.substring(18, 21),
    );
    if (component == "BATT") {
      this.checkBattery(rawValue, time)
      this.battStatusLog.set(time, rawValue)
    } else {
      this.checkThermostat(rawValue, time)
      this.tstatStatusLog.set(time, rawValue)
    }
  }

  /** Function checkBattery takes a battery reading raw value and timestamp to 
   *  check if it constitutes a limit violation. */
  checkBattery(rawVal, time) {
    if (rawVal < index.battLimit) {
      let added = false;
      while (added != true) {
        if (this.battViolatons.length == 0) {
          this.battViolatons.push(time);
          added = true;
        } else if (Math.round(((time - this.battViolatons[0] % 86400000) % 3600000) / 60000) < 5) {
          this.battViolatons.push(time);
          added = true;
        } else if (ath.round(((time - this.battViolatons[0] % 86400000) % 3600000) / 60000) > 5) {
          this.battViolatons.shift()
        }
      }
    }
    
    if (this.battViolatons.length == 3) {
      this.alertMessage("RED LOW", "BATT", this.battViolatons[0])
    }
  }

  /** Function checkThermostat takes a thermostat reading raw value and timestamp
   *  to check if it constitutes a limit violation. */
  checkThermostat(rawVal, time) {
    if (rawVal > index.tstatLimit) {
      let added = false;
      while (added != true) {
        if (this.tstatViolations.length == 0) {
          this.tstatViolations.push(time);
          added = true;
        } else if (Math.round(((time - this.tstatViolations[0] % 86400000) % 3600000) / 60000) < 5) {
          this.tstatViolations.push(time);
          added = true;
        } else if (ath.round(((time - this.tstatViolations[0] % 86400000) % 3600000) / 60000) > 5) {
          this.tstatViolations.shift()
        }
      }
    }
    
    if (this.tstatViolations.length == 3) {
      this.alertMessage("RED HIGH", "TSAT", this.tstatViolations[0])
    }
  }

  /** Function alertMessage takes the severity/violation, component, and timestamp
   * and returns an output alert message.
   */
  alertMessage(severity, component, time) {
    let milliseconds = time.getMilliseconds()
    if (milliseconds < 10) {
      milliseconds = "00" + milliseconds
    } else if (milliseconds < 100) {
      milliseconds = "0" + milliseconds
    }

    let timeFormatted = 
       time.getFullYear() + "-" + 
      (time.getMonth()<10?'0':'') + time.getMonth() + "-" + 
      (time.getDate()<10?'0':'') + time.getDate() + "T" + 
      (time.getHours()<10?'0':'') + time.getHours() + ":" + 
      (time.getMinutes()<10?'0':'') + time.getMinutes() + ":" + 
      (time.getSeconds()<10?'0':'') + time.getSeconds() + "." + 
       milliseconds + "Z"
    
    let alert = {
      "satelliteId": this.id,
      "severity": severity,
      "component": component,
      "timestamp": timeFormatted
    }
    console.log(alert);
    this.alerts.push(alert)
  }
}

module.exports = Satellite